/**Basic arithmetic operations */

const mylib = {
    /**Multiline arrow function. */
    add: (a,b) =>{
        const sum = a + b;
        return sum;
    },
    subtract: (a,b) =>{
        return a -b;   
    },
    /**Singleline arrow function. */
    divide: (divident,divisor) => divident/divisor,
    multiply: function(a,b){
        return a * b;
    }
};

module.exports = mylib;